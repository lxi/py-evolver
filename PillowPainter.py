from PIL import Image, ImageDraw
import numpy
import primitives
from tools import switch, f2i


class PillowPainter(object):
    """Paint a scene into and image using Pillow."""
    def __init__(self, size):
        self.canvas_size = size
        self.bgcolor = (0, 0, 0)
        self.image = Image.new('RGB', self.canvas_size, self.bgcolor)
        self.dc = ImageDraw.Draw(self.image, 'RGBA')

    def paint_shape(self, shape):
        """Paint a shape according to its type."""

        color = f2i(shape.get_rgba())

        for case in switch(type(shape)):
            if case(primitives.Square):
                pass    # fallthrough to polygon
            if case(primitives.Polygon):
                self.dc.polygon(list(shape.vertices.flatten()),
                                outline=color, fill=color)
                break

            if case(primitives.Circle):
                ox, oy = shape.center
                self.dc.ellipse([ox - shape.radius, oy - shape.radius,
                                 ox + shape.radius, oy + shape.radius],
                                outline=color, fill=color)
                break

            if case(primitives.Line):
                self.dc.line(list(shape.points.flatten()),
                             fill=color, width=int(shape.width))
                break

            if case(primitives.Rectangle):
                self.dc.rectangle([shape.x1, shape.y1, shape.x2, shape.y2],
                                  fill=color, outline=color)
                break

            if case():
                raise NotImplementedError("No painting function implemented")

    def paint_scene(self, shapes):
        # black background
        self.image = Image.new('RGB', self.canvas_size, self.bgcolor)
        self.dc = ImageDraw.Draw(self.image, 'RGBA')

        for shape in shapes:
            self.paint_shape(shape)

        return self.image

    def get_rgb_data(self):
        imgdata = numpy.array(self.image.getdata())
        return imgdata

    def image2wxbitmap(self, image):
        import wx
        wximage = wx.EmptyImage(image.size[0], image.size[1])
        wximage.SetData(image.convert("RGB").tobytes())
        return wx.BitmapFromImage(wximage)

    def load_png(self, fname):
        return Image.load(fname)
