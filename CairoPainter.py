import cairo
import numpy
import primitives
from tools import switch


class CairoPainter(object):
    """Paint a scene into an image using pycairo. This is a lot faster than
    Pillow."""
    def __init__(self, size):
        self.canvas_size = size
        w, h = self.canvas_size

        # create default image and drawing context
        self.image = cairo.ImageSurface(cairo.FORMAT_RGB24, w, h)
        self.dc = cairo.Context(self.image)

    def paint_shape(self, shape):
        """Paint a shape according to its type."""
        # set pen color
        # cairo uses RGBA values between [0.0, 1.0]
        rgba = shape.get_rgba()
        self.dc.set_source_rgba(*rgba)

        # switch-case construct inspired by the Internet
        # uglier than letting the shapes handle their own painting, but makes
        # supporting several graphics libraries easier
        for case in switch(type(shape)):
            if case(primitives.Circle):
                x, y = shape.center
                self.dc.arc(x, y, shape.radius, 0, 2 * numpy.pi)
                self.dc.fill()
                break

            if case(primitives.Polygon):
                # start from the last so we finish there and close the shape
                self.dc.move_to(*shape.vertices[-1])
                for p in shape.vertices:
                    self.dc.line_to(*p)

                self.dc.fill()
                break

            if case(primitives.Line):
                self.dc.set_line_width(shape.width)
                (x0, y0), (x1, y1) = shape.points
                self.dc.move_to(x0, y0)
                self.dc.line_to(x1, y1)
                self.dc.stroke()
                break

            if case(primitives.CrosshatchLine):
                self.dc.set_line_width(shape.width)
                self.dc.move_to(*shape.p1)
                self.dc.line_to(*shape.p2)
                self.dc.stroke()
                break

            if case(primitives.Rectangle):
                self.dc.rectangle(shape.x1, shape.y1,
                                  shape.x2 - shape.x1, shape.y2 - shape.y1)
                self.dc.fill()
                break

            if case(primitives.Square):
                self.dc.move_to(*shape.vertices[-1])

                for x, y in shape.vertices:
                    self.dc.line_to(x, y)

                self.dc.fill()
                break

            if case():
                raise NotImplementedError("No paint function implemented")

    def paint_scene(self, shapes):
        w, h = self.canvas_size
        self.image = cairo.ImageSurface(cairo.FORMAT_RGB24, w, h)
        self.dc = cairo.Context(self.image)

        # paint a black background
        self.dc.set_source_rgb(0.0, 0.0, 0.0)
        self.dc.rectangle(0, 0, w, h)
        self.dc.fill()

        for shape in shapes:
            self.paint_shape(shape)

        return self.image

    def get_rgb_data(self):
        """Return the image data as RGB triplets in a 2-D NumPy array."""
        data = numpy.frombuffer(self.image.get_data(), numpy.uint8)

        # reshape isn't mandatory if we're not discriminating between channels
        # it makes the whole a bit more understandable though
        data = data.reshape((len(data) / 4, 4))
        return data[:, 2::-1]

    def image2wxbitmap(self, image):
        """Convert an image of this type to a wx bitmap."""
        import wx.lib.wxcairo
        return wx.lib.wxcairo.BitmapFromImageSurface(image)

    def load_png(self, fname):
        return cairo.ImageSurface.create_from_png(fname)
